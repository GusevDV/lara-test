require('./bootstrap');

 /*--------------------------------------------------------------
 # Сообщения об ошибках при валидации формы
 --------------------------------------------------------------*/

 jQuery.extend(jQuery.validator.messages, {
     required: "Обязательное поле",
     remote: "Пожалуйста, исправьте это поле",
     email: "Введите корректный email",
     url: "Введите корректный URL.",
     number: "Введите корректное число",
     digits: "Пожалуйста, введите только числа",
     equalTo: "Please enter the same value again.",
     accept: "Please enter a value with a valid extension.",
     maxlength: jQuery.validator.format("Максимальное количество символов: {0}"),
     minlength: jQuery.validator.format("Минимальное количество символов: {0}"),
     rangelength: jQuery.validator.format("Введите значение более {0} и менее {1} символов"),
     range: jQuery.validator.format("Please enter a value between {0} and {1}."),
     max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
     min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
 });


 jQuery(document).ready(function(){

 var form = $("#UserForm");
 form.validate({
   rules: {
       email: {
         email: true,
         minlength: 2,
         required: true,
       },
       password: {
         minlength: 4,
       },
       name: {
         maxlength: 255,
       },
       second_name: {
         maxlength: 255,
       },
       surname: {
         maxlength: 255,
       },
       date: {
         date: true
       },
       city_id: {
         min: 1,
         number: true
       },
       role_id: {
         min: 1,
         number: true
       },
     },
     messages: {
         parent_cats: {
             required: "Поле должно быть заполнено"
         }
     },
     highlight: function(element) {
       $(element).closest('.form-group').addClass('has-error');
     },
     unhighlight: function(element) {

         $(element).closest('.form-group').removeClass('has-error');

     },
     success: function(label) {
       label.remove();
     },
     errorPlacement: function(error, element) {

    }
 });

 $(".adduser-password").rules("add", {
   required:true,
 });

 });
