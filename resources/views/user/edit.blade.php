@extends('main')

@section('content')
<div class="row">
  <div class="col-md-6 col-md-offset-3">
  <div class="panel panel-default">
    <h2 class="text-center">Добавить пользователя</h2>
  <div class="panel-body">
    {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put', 'files' => false, 'id'=>'UserForm']) !!}
          <div class="form-group">
            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
          </div>
          <div class="form-group">
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Пароль']) !!}
          </div>
          <div class="form-group">
            {!! Form::text('surname', null, ['class' => 'form-control', 'placeholder' => 'Фамилия']) !!}
          </div>
          <div class="form-group">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
          </div>
          <div class="form-group">
            {!! Form::text('second_name', null, ['class' => 'form-control', 'placeholder' => 'Отчество']) !!}
          </div>
          <div class="form-group">
            {!! Form::date('birth_date', Carbon\Carbon::parse($user->birth_date)->format('Y-m-d'),  ['class' => 'form-control']); !!}
          </div>

          <div class="form-group">
              {!! Form::select('city_id', $cities_list, null, ['class' => 'form-control']); !!}
          </div>
          <div class="form-group">
              {!! Form::select('role_id', $roles_list, null, ['class' => 'form-control', 'multiple' => true]); !!}
          </div>
          <div class="form-group">
              {!! Form::submit('ОТПРАВИТЬ', ['class' => 'btn btn-primary']) !!}
          </div>

    {!! Form::close() !!}
    @if($errors->any())

        @foreach($errors->all() as $error)
          <div class="alert alert-danger" role="alert">
            <span class="sr-only">Ошибка:</span>
              {{$error}}
          </div>
        @endforeach

      @endif
  </div>
  </div>
  </div>
</div>
@stop
@section('scripts')
  <script src="{{ elixir('/js/app.js') }}"></script>
@stop
