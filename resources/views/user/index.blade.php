@extends('main')

@section('content')
  <div class="row">
    <div class="col-md-10"><h1>Список пользователей</h1></div>
    <div class="col-md-2 b-add-user"><a href="{{ route('user.create')}}"class="btn btn-success btn-add-user">ДОБАВИТЬ</a></div>
    <div class="col-md-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Email</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Дата рождения</th>
            <th>Город</th>
            <th>Роль</th>
          </tr>
        </thead>
        <tbody>

          @foreach($users as $user)

           <tr>
             <td><a href="{{ route('user.edit', $user->id)}}">{{$user->email}}</a></td>
             <td>{{$user->surname}}</td>
             <td>{{$user->name}}</td>
             <td>{{$user->second_name}}</td>
             <td>{{Carbon\Carbon::parse($user->birth_date)->format('d/m/Y')}}</td>
             <td>
               {{$user->city->name}}
             </td>
             <td>
               {{$user->role->name}}
             </td>
           </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="col-md-12">
      <div class="box-footer">
        {{ $users->links() }}
      </div><!-- box-footer -->
    </div>
  </div>

@stop
