<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="favicon.png" type="image/png"/>
  <title>Тестовый сайт</title>
  <link href="{{ elixir('/css/app.css')}}" rel="stylesheet">
  @yield('styles')
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="/js/html5shiv.min.js"></script>
    <script src="/js/respond.min.js"></script>
  <![endif]-->
</head>
<body>
    <!-- Header -->
    @include('parts.header')
    <!-- Content -->

    <div class="container">
    <?php // @include('layout.errors') ?>
        @if (Session::has('flash_notification.message'))
          @include('flash::message')
        @endif
        @yield('content')

    </div>

    <!-- Footer -->
    @yield('scripts')
</body>
</html>
