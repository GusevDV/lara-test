<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Email: {{$user->email}}<br>
      Имя: {{$user->name}}<br>
      Отчество: {{$user->second_name}}<br>
      Фамилия: {{$user->surname}}<br>
      Дата рождения: {{Carbon\Carbon::parse($user->birth_date)->format('d/m/Y')}}<br>
			Город: {{$user->city->name}}<br>
			Роль: {{$user->city->role}}<br>
		</div>
	</body>
</html>
