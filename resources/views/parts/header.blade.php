<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle</span>
      </button>
      <a class="navbar-brand" href="/">LaraTest</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        @foreach ($links as $link => $value)
          <li><a href="{{$link}}">{{$value}}</a></li>
        @endforeach
      </ul>
    </div>

  </div>
</nav>
