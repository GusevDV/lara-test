<?php

use Illuminate\Database\Seeder;
use App\UserRole;
use App\City;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CitiesTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->command->info('Success!');
    }
}
class UserRolesTableSeeder extends Seeder {
  public function run()
  {
    DB::table('user_roles')->delete();

    UserRole::create(['name' => 'Администратор', 'description' => 'Глобальные привелегии']);
    UserRole::create(['name' => 'Модератор', 'description' => 'Изменение/удалении пользователей и комментариев']);
    UserRole::create(['name' => 'Редактор', 'description' => 'Создание/изменение контента']);
    UserRole::create(['name' => 'Подписчик', 'description' => 'Чтение контента']);
  }
}
class UserTableSeeder extends Seeder {

  public function run()
  {

    $data_seeder = config('seeder');
    $names = $data_seeder['names'];
    $second_names = $data_seeder['second_names'];
    $surnames = $data_seeder['surnames'];
    $cities = $data_seeder['cities'];

    $user = [];
    for ($i = 0; $i <= 10; $i++){
      $users[] =[
          'name' => $names[array_rand($names)],
          'email' => str_random(10).'@gmail.com',
          'password' => bcrypt('secret'),
          'second_name' => $second_names[array_rand($second_names)],
          'surname' => $surnames[array_rand($surnames)],
          'city_id' => array_rand($cities),
          'birth_date' => Carbon::now(),
          'role_id' => 4,
      ];
    }
    DB::table('users')->insert($users);
  }
}


class CitiesTableSeeder extends Seeder {



  public function run()
  {
    $data_seeder = config('seeder');

    DB::table('cities')->delete();

    foreach ($data_seeder['cities'] as $city) {
        City::create($city);
  }

  }

}
