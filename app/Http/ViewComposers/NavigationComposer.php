<?php
namespace App\Http\ViewComposers;


use Illuminate\Contracts\View\View;

class NavigationComposer{

  protected $links;

  public function __construct()
  {
    $this->links = config('nav');
  }

  public function compose(View $view)
  {
    $view->with('links', $this->links);
  }
}
