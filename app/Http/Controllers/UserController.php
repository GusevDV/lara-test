<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\City;
use App\UserRole;
use App\Message;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Laracasts\Flash\Flash;
use App\Events\UserCreated;

class UserController extends Controller
{
  public function index()
  {
    $users = User::orderBy('created_at', 'desc')->paginate(20);
    return view('user.index')->with('users', $users);
  }

  public function create()
  {

    $cities = City::orderBy('name', 'asc')->get();
    $roles = UserRole::all();

    foreach ($cities as $city) {
      $cities_list[$city->id] = $city->name;
    }

    foreach ($roles as $role) {
      $roles_list[$role->id] = $role->name;
    }

    return view('user.create')
      ->with('roles_list', $roles_list)
      ->with('cities_list', $cities_list);
  }

  public function store(UserStoreRequest $request)
  {
      $user = new User();
      $log = new Message();
      $user->email = $request->input('email');
      $user->password = bcrypt($request->input('password'));
      $user->name = $request->input('name');
      $user->second_name = $request->input('second_name');
      $user->surname = $request->input('surname');
      $user->birth_date = $request->input('birth_date');
      $user->city_id = $request->input('city_id');

      if ($request->input('role_id') != '')
      {
          $user->role_id = $request->input('role_id');
      }
      else
      {
        $user->role_id = 4;
      }

      $user->save();

      $log->message = "User has been added. User id: ".$user->id;

      $log->save();

      event(new UserCreated($user));


      return redirect()->route('home');
  }

  public function edit($id)
  {
      $user = User::find($id);
      $cities = City::orderBy('name', 'asc')->get();
      $roles = UserRole::all();

      foreach ($cities as $city) {
        $cities_list[$city->id] = $city->name;
      }

      foreach ($roles as $role) {
        $roles_list[$role->id] = $role->name;
      }

      return view('user.edit')
      ->with('user', $user)
      ->with('roles_list', $roles_list)
      ->with('cities_list', $cities_list);
  }

  public function update(UserUpdateRequest $request, $id)
  {
      $user = User::find($id);
      $user->email = $request->input('email');

      if ($request->input('password') != '') {
              $user->password = bcrypt($request->input('password'));
      }

      $user->name = $request->input('name');
      $user->second_name = $request->input('second_name');
      $user->surname = $request->input('surname');
      $user->birth_date = $request->input('birth_date');
      $user->city_id = $request->input('city_id');

      if ($request->input('role_id') != '')
      {
          $user->role_id = $request->input('role_id');
      }
      else
      {
        $user->role_id = 4;
      }

      $user->save();

      Flash::success('Информация успешно обновлена');

      return redirect()->back();

  }



}
