
## Installation

Clone this repository.

Run `composer install`

Run `npm install`

Migrate tables:

Run `php artisan migrate`

Database seeding:

Run `php artisan db:seed`
